<?php
namespace application\controllers;
use \library as h;
use \library\MVC as l;
use \application\models as m;

class lostpass extends l\Controller {
    private $uid;
    private $val_key;

    private $_modelUser;
    private $_modelUserLostPass;
    private $_mail;
    private $redis = null;

    function __construct() {
        parent::__construct();
		$this->redis = $this->getRedis();
    }

    public function keyAction($uid = null, $key = null) {
		header("Content-type: application/json");
		$resp = self::RESP;
		$method = h\httpMethodsData::getMethod();
		$data = h\httpMethodsData::getValues();

		if($method !== 'post' && $method !== 'get') {
			$resp['code'] = 405; // Method Not Allowed
		}
		elseif($this->isLogged() === false) {
			if($method === 'get' && $uid !== null && $key !== null) {
				$data->uid = $uid;
				$data->key = $key;
			}

			if(isset($data->uid) && isset($data->key) && is_pos_digit($data->uid) && strlen($data->key) >= 128) {
        		$this->uid = intval($data->uid);
        		$this->val_key = $data->key;
        		$this->_modelUserLostPass = new m\UserLostPass($this->uid);

		        if($key = $this->_modelUserLostPass->getKey()) { // Found key
			        if($key === $this->val_key && $this->_modelUserLostPass->getExpire() >= time()) {
						// Same keys
                        $pass = (new m\Users($this->uid))->getPassword();
						$resp['code'] = 200;
						$resp['status'] = 'success';
						$resp['message'] = 'ok';
                        $resp['data']['onePassword'] = $pass !== false ? strlen(strstr($pass, ':')) > 1 : false;
			        } else { // Different key, send a new mail ?
			            $resp['message'] = 'differentKey';
			        }
				}
			} else {
				$resp['message'] = 'emptyField';
			}
		}

		http_response_code($resp['code']);
		echo json_encode($resp);
    }

    public function mailAction() {
    	// Send lost pass mail with validation key
		header("Content-type: application/json");
		$resp = self::RESP;
		$method = h\httpMethodsData::getMethod();
		$data = h\httpMethodsData::getValues();

		if($method !== 'post') {
			$resp['code'] = 405; // Method Not Allowed
		}
		elseif($this->isLogged() === false) {
			if((isset($data->uid) && is_pos_digit($data->uid)) || isset($data->username)) {
				if(isset($data->username)) {
					$new_user = new m\Users();
					if(filter_var($data->username, FILTER_VALIDATE_EMAIL) === false) {
						$new_user->login = $data->username;
					} else {
						$new_user->email = $data->username;
					}
					$uid = $new_user->getId();
				} else {
					$uid = $data->uid;
				}

				if($uid !== false) {
					$this->uid = intval($uid);
					$this->_modelUser = new m\Users($this->uid);

					if($user_mail = $this->_modelUser->getEmail()) {
                        self::loadLanguage();
						$resp['code'] = 200;
						$resp['status'] = 'success';

						$key = hash('sha512', uniqid(rand(), true));

						$this->_modelUserLostPass = new m\UserLostPass($this->uid);
						$new = $this->_modelUserLostPass->getKey() ? false : true;
						$this->_modelUserLostPass->val_key = $key;
						$this->_modelUserLostPass->expire = time()+3600;

						if($new) {
							$this->_modelUserLostPass->Insertion();
						} else {
							$this->_modelUserLostPass->Update();
						}

						$this->_mail = new l\Mail();
						$this->_mail->delay(60, $this->uid, $this->redis, 'lostpass');
						$this->_mail->_to = $user_mail;
						$this->_mail->_subject = self::$txt->LostPass->subject;
						$this->_mail->_message = str_replace(
							["[id_user]", "[key]", "[url_app]"],
							[$this->uid, $key, URL_APP],
							self::$txt->LostPass->message
						);
						$resp['data'] = $this->uid;
						$resp['message'] = $this->_mail->send(); // 'sent' or 'wait'
					}
				} else {
					$resp['message'] = 'unknownUser';
				}
            } else {
				$resp['message'] = 'emptyField';
			}
        }

		http_response_code($resp['code']);
		echo json_encode($resp);
    }

	public function DefaultAction() {
		header("Content-type: application/json");
		$resp = self::RESP;
		$method = h\httpMethodsData::getMethod();
		$data = h\httpMethodsData::getValues();

		if($method !== 'post') {
			$resp['code'] = 405; // Method Not Allowed
		}
		elseif($this->isLogged() === false) {
			if(isset($data->uid) && isset($data->key) && is_pos_digit($data->uid) && strlen($data->key) >= 128 && (isset($data->password) || isset($data->cek))) {
				$this->_modelUserLostPass = new m\UserLostPass($data->uid);
				if($key = $this->_modelUserLostPass->getKey()) {
					if($key === $data->key && $this->_modelUserLostPass->getExpire() >= time()) {
						$this->_modelUser = new m\Users($data->uid);
                        $pass = $this->_modelUser->getPassword();
                        if ($pass !== false) {
                            $salt = strstr($pass, ':');
                            $onePassword = $salt !== false && strlen($salt) > 1;
                            $cek = isset($data->cek) ? htmlspecialchars($data->cek) : null;
                            if (!$onePassword || ($onePassword && isset($data->salt) && isset($data->cek) && isset($data->password))) {
                                if (!$onePassword && !isset($data->password) && isset($cek)) { // Update CEK only
                                    $this->_modelUser->cek = $cek;
                                    $update = $this->_modelUser->updateCek();
                                } elseif (isset($data->password)) { // Update password and eventually the CEK
        						    $this->_modelUser->password = password_hash(urldecode($data->password), PASSWORD_BCRYPT);
                                    // Add the new salt for One-Password mode
                                    if ($onePassword && isset($data->salt)) $this->_modelUser->password .= ':'.htmlspecialchars($data->salt);
                                    $update = $this->_modelUser->updatePassword($cek);
                                }
        						if ($update) { // We updated password and/or cek
        							$this->_modelUserLostPass->Delete();
        							$this->redis->del('uid:'.$this->uid.':mailnbf:lostpass');
                                    if ($onePassword || $cek !== null) { // Reset user data
                                        $userFolder = NOVA.'/'.str_replace(['.', '/'], '', $data->uid);
                                        if ($userFolder !== NOVA.'/') {
                                            $command = 'rm -rf "'.$userFolder.'" &>/dev/null &';
                                            shell_exec($command);
                                        }
                                        mkdir(NOVA.'/'.$data->uid, 0770);
                                        (new m\Files($data->uid))->deleteFilesfinal();
                                        (new m\Folders($data->uid))->deleteFoldersfinal();
                                        (new m\Storage($data->uid))->updateSizeStored(0);
                                        $this->removeTokens($data->uid);
                                    }
        							$resp['code'] = 200;
        							$resp['status'] = 'success';
        							$resp['message'] = 'updated';
        						}
                            }
                        }
					} else {
			            $resp['message'] = 'differentKey';
			        }
				}
			} else {
				$resp['message'] = 'emptyField';
			}
		}

		http_response_code($resp['code']);
		echo json_encode($resp);
	}
}
